﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
    [SerializeField] private int timeToFinish = 10;
    [SerializeField] private int scoreTowin = 10;
    [SerializeField] private string NextScene;
    [SerializeField] private Transform player;
    [SerializeField] private string GameOverScene;
    [SerializeField] private string MenuScene;
    private VidaPlayer vida;
    private static LevelManager instance;
    public static LevelManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LevelManager>();
                if (instance == null)
                {
                    GameObject go = new GameObject("ScoreManager");
                    instance = go.AddComponent<LevelManager>();
                }
            }
            return instance;
        }
    }

    
    private void Awake()
    {
        vida = player.GetComponent<VidaPlayer>();
        Invoke("FinishLevel", timeToFinish);
    }

    void FinishLevel()
    {
        if (ScoreManager.Instance.Score >= scoreTowin)
        {
            SceneManager.LoadScene(NextScene);
        }
        else
        {
            SceneManager.LoadScene(GameOverScene);
        }
    }

    private void Update()
    {
        if(vida.VidaActual <= 0)
        {
            SceneManager.LoadScene(GameOverScene);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CancelInvoke();
            SceneManager.LoadScene(MenuScene);
        }
    }
}
