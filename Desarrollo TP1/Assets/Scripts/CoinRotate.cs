﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRotate : MonoBehaviour {

    [SerializeField] float rotateSpeed = 5f;

	void Update () {
        transform.Rotate(transform.up * rotateSpeed * Time.deltaTime);
	}
}
