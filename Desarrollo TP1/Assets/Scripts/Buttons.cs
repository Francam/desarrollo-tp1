﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour {
    [SerializeField] string sceneToLoad = "MainLevel";

    public void StartGame() {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void ExitGame() {
        Application.Quit();
    }

}