﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Campfire : MonoBehaviour {

    [SerializeField] int daño;
    private bool onFire = false;
    [SerializeField] int damageWait;
    private VidaPlayer vida;
    private float timer;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "CactusCharacter") {
            vida = other.GetComponent<VidaPlayer>();
            onFire = true;
            vida.VidaActual -= daño;
            timer = 0;
        }
    }

    private void OnTriggerExit(Collider other) {
        onFire = false;
    }
    private void Update()
    {
        timer += Time.deltaTime;
        if (onFire && timer >= damageWait) {
            vida.VidaActual -= daño;
            timer = 0;
        }
    }
}
