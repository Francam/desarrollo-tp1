﻿using UnityEngine;

public class ApplePickup : MonoBehaviour {

    [SerializeField] int healAmount = 15;
    AudioSource pickupAudio;
    bool destroyed = false;

    private void Awake()
    {
        pickupAudio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other) {
        if (!destroyed)
        {
            if (other.gameObject.tag == "CactusCharacter")
            {
                PlayerMove pm = other.GetComponent<PlayerMove>();
                VidaPlayer hp = other.GetComponent<VidaPlayer>();
                hp.VidaActual += healAmount;
                if (hp.VidaActual == hp.VidaMaxima)
                    pm.PowerUpSpeed();
                ScoreManager.Instance.Score += 1;
            }
            destroyed = true;
            pickupAudio.Play();
            Invoke("DestroyAfterAudio", 0.5f);
        }
    }
    void DestroyAfterAudio()
    {
        Destroy(gameObject);
    }
}
