﻿using UnityEngine;

public class ThornMove : MonoBehaviour {

    [SerializeField] float velocidad = 5;

    public void Update()
    {
        transform.Translate(0, 0, velocidad * Time.deltaTime);
    }
}
