﻿using UnityEngine;

public class MushroomBulletDamage : MonoBehaviour {

    [SerializeField] float daño;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "CactusCharacter")
        {
            VidaPlayer vida = other.GetComponent<VidaPlayer>();
            if (vida)
            {
                vida.VidaActual -= daño;
            }
        }
        Destroy(gameObject);
    }
    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
