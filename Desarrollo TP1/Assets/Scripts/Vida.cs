﻿using UnityEngine;


public class Vida : MonoBehaviour {
    [SerializeField] float vidaActual;
    [SerializeField] float vidaMaxima;
    public float VidaMaxima
    {
        get { return vidaMaxima; }
    }

    private void Awake()
    {
        vidaMaxima = vidaActual;
    }
    public float VidaActual
    {
        get { return vidaActual; }
        set
        {
            vidaActual = value;
            if (vidaActual <= 0)
            {
                vidaActual = 0;
                SendMessage("OnDeath", SendMessageOptions.DontRequireReceiver);
                Destroy(gameObject);
            }
            else if (vidaActual >= 100)
            {
                vidaActual = 100;
            }
        }
    }
}