﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomFollow : MonoBehaviour {

    [SerializeField] GameObject target = null;
    [SerializeField] float speed = 3.0f;
    Vector3 lastKnownPosition = Vector3.zero;
    Quaternion lookAtRotation;

    void Update()
    {
        if (target)
        {
            if (lastKnownPosition != target.transform.position)
            {
                lastKnownPosition = target.transform.position;
                lookAtRotation = Quaternion.LookRotation(lastKnownPosition - transform.position);
            }

            if (transform.rotation != lookAtRotation)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, lookAtRotation, speed * Time.deltaTime);
            }
        }
    }
}
