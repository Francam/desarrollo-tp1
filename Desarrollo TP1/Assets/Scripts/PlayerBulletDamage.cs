﻿using UnityEngine;

public class PlayerBulletDamage : MonoBehaviour
{
    [SerializeField] float daño;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Mushroom")
        {
            Vida vida = other.GetComponent<Vida>();
            if (vida)
            {
                vida.VidaActual -= daño;
            }
        }
        Destroy(gameObject);
    }
}
