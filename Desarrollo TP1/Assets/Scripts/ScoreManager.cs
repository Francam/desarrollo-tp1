﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    [SerializeField] Text scoreText;
    private static ScoreManager instance;
    [SerializeField] int score;
    public static ScoreManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<ScoreManager>();
                if (instance == null) //Si no encontre el instance
                {
                    GameObject go = new GameObject("ScoreManager");
                    instance = go.AddComponent<ScoreManager>();
                }
            }
            return instance;
        }
    }

    public int Score
    {
        get { return score; }
        set {
            score = value;
            scoreText.text = "Score: " + score;
        }
    }
}
