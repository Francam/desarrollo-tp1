﻿using UnityEngine;

public class ThornBullet : MonoBehaviour {

    [SerializeField] int timeAlive;
    void Awake()
    {
        Invoke("TimeDestroy", timeAlive);
    }
    void TimeDestroy()
    {
        Destroy(gameObject);
    }
}
