﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VidaPlayer : MonoBehaviour {

    [SerializeField] float vidaActual;
    [SerializeField] float vidaMaxima;
    float flashSpeed = 5f;
    [SerializeField] Slider healthSlider;
    [SerializeField] Image damageImage;
    Color redFlash = new Color(1f, 0f, 0f, 0.5f);
    Color yellowFlash = new Color(0f, 1f, 0, 0.5f);
    public float VidaMaxima
    {
        get { return vidaMaxima; }
    }

    private void Awake()
    {
        VidaActual = vidaMaxima;
    }
    public float VidaActual
    {
        get { return vidaActual; }
        set
        {
            if (vidaActual <= value)
            {
                damageImage.color = yellowFlash;
                //Invoke("ClearColor", 0.5f);
            }
            else
            {
                damageImage.color = redFlash;
                //Invoke("ClearColor", 0.5f);
            }
            vidaActual = value;
            if (vidaActual <= 0)
            {
                vidaActual = 0;
                //Destroy(gameObject);
            }
            else if (vidaActual >= 100)
            {
                vidaActual = 100;
            }
            healthSlider.value = vidaActual;
        }
    }
    void ClearColor()
    {
        damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
    }
    private void Update()
    {
        ClearColor();
    }
}
