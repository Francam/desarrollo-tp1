﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScore : MonoBehaviour {

    [SerializeField] int scorePerCoin = 1;
    [SerializeField] float timeToDestroy = 0.7f;
    AudioSource coinPickup;
    bool coinAlive = true;

    private void Awake()
    {
        coinPickup = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (coinAlive)
        {
            ScoreManager.Instance.Score += scorePerCoin;            
            coinPickup.Play();
            Invoke("DestroyItem", timeToDestroy);
        }
    }
    void DestroyItem()
    {
        Destroy(gameObject);
    }
}

