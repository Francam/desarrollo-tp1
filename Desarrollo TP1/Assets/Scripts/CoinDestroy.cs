﻿using UnityEngine;

public class CoinDestroy : MonoBehaviour
{
    [SerializeField] float destroyTime;
    void Awake()
    {
        Invoke("DestroyTime", destroyTime);
    }
    private void DestroyTime()
    {
        Destroy(gameObject);
    }
}
