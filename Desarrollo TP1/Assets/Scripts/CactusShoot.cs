﻿using UnityEngine;

public class CactusShoot : MonoBehaviour {

    [SerializeField] GameObject prefab;
    [SerializeField] float timeBetweenShot = 0.3f;
    Animator anim;
    float timer;
    private void Start()
    {
        anim = GetComponentInParent<Animator>();
    }
    void Update()
    {
        timer += Time.deltaTime;
        if(timer > timeBetweenShot) {
            anim.SetBool("attacking", false);
        }
        if (Input.GetMouseButtonDown(0)) {
            if (timer >= timeBetweenShot) {
                timer = 0;
                Instantiate(prefab, transform.position, transform.rotation);
                anim.SetBool("attacking", true);
            }
        }
    }
}
