﻿using UnityEngine;

public class PlayerMove : MonoBehaviour {

    [SerializeField] float speed;
    [SerializeField] float jumpForce;
    [SerializeField] float gravity;
    [SerializeField] float lookSpeed = 2f;
    [SerializeField] float soundDelay = 0.4f;

    private float normalSpeed;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController cc;
    private Animator anim;
    private AudioSource sound;
    private bool moving = false;
    private float jumping;    
    private float timer = 0;
    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    private void Awake() {
        cc = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
        normalSpeed = speed;
    }

    void UpdateAnim()
    {
        if (jumping > 0) {
            anim.SetBool("jumping", true);
        }
        else {
            anim.SetBool("jumping", false);
        }
        anim.SetBool("walking", moving);
        //anim.SetBool("attacking", );
    }

    void Update () {
        if (moving && sound.isPlaying == false) {
            timer += Time.deltaTime;
            if (timer >= soundDelay) {
                timer = 0;
                sound.volume = Random.Range(0.8f, 1);
                sound.pitch = Random.Range(0.8f, 1.5f);
                sound.Play();
            }
        }
        UpdateCameraTurn();
        UpdateMovement();
        Jump();
        UpdateGravity();
        UpdateAnim();
        cc.Move(moveDirection * Time.deltaTime);
    }

    void UpdateGravity() {
        jumping -= gravity * Time.deltaTime;
    }

    void Jump() {
        if (cc.isGrounded) {
            if (Input.GetButton("Jump")) {
                jumping = jumpForce;
            }
        }
    }

    void UpdateCameraTurn() {
        Vector3 look = Vector3.zero;
        look.y = Input.GetAxis("Mouse X") * lookSpeed;
        transform.Rotate(look);
    }

    void UpdateMovement() {
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
            moving = true;
        }
        else {
            moving = false;
        }
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        input = transform.TransformDirection(input);

        float spd = speed;
        if (Input.GetKey(KeyCode.LeftShift))
            spd *= 2;
        input *= spd;

        if (!cc.isGrounded)
        {
            input *= Time.deltaTime * 2;
            moveDirection += input;
        }
        else
        {
            moveDirection = input;
        }

        moveDirection.y = 0;
        if (moveDirection.magnitude > spd)
        {
            moveDirection = moveDirection.normalized * spd;
        }
        moveDirection.y = jumping;
    }

    public void PowerUpSpeed() {
        if (speed != normalSpeed * 2) {
            speed = normalSpeed * 2;
            Invoke("RmeoveSpeed", 5);
        }
    }

    void RemoveSpeed() {
        speed = normalSpeed;
    }
}
