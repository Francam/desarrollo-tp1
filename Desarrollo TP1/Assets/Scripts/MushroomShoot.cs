﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomShoot : MonoBehaviour {

    [SerializeField] GameObject prefab;
    bool inShootingRange = false;
    float timer = 0;
    
    float timeBetweenShoot = 0.5f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "CactusCharacter") {
            inShootingRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "CactusCharacter") {
            inShootingRange = false;
        }
    }

    void Update () {
        timer += Time.deltaTime;
        if (inShootingRange) {
            if (timer > timeBetweenShoot) {
                Instantiate(prefab, transform.position, transform.rotation);
                timer = 0;
            }    
        }
    }
}