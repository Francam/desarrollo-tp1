﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class EnemigoSpider : MonoBehaviour {
    
    [SerializeField] Transform target;
    [SerializeField] private int damage = 50;
    private bool walk;
    private bool jump;
    private bool attack;
    Animator anim;
    private NavMeshAgent nma;
    public Transform[] positions;
    private int currentWaypoint;
    private float timer;
    
    [SerializeField]
    LayerMask obstacleMask;
    public float viewRadius;
    [Range(0, 360)]
    public float viewAngle;

    void Awake () {
        anim = GetComponent<Animator>();
        nma = GetComponent<NavMeshAgent>();
        currentWaypoint = 0;
        nma.destination = positions[currentWaypoint].position;
        jump = false;
        attack = false;
        walk = true;
    }

    void Update()
    {
        float dist = (target.position - transform.position).magnitude;
        Vector3 dirToTarget = (target.position - transform.position).normalized;
        if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2 && dist <= viewRadius)
        {
            float dstToTarget = Vector3.Distance(transform.position, target.position);
            if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
            {
                nma.destination = target.position;
            }
        }
        else
        {
            attack = false;
            if (!nma.pathPending && nma.remainingDistance <= nma.stoppingDistance)
            {
                currentWaypoint++;
                currentWaypoint %= positions.Length;
                nma.destination = positions[currentWaypoint].position;
            }
        }

        anim.SetBool("Walk", walk);
        anim.SetBool("Jump", jump);
        anim.SetBool("Attack", attack);

        if (transform.position.y < -60)
        {
            Destroy(gameObject);
        }
    }
    void OnTriggerEnter(Collider c)
    {
        if (c.tag == "CactusCharacter")
        {
            VidaPlayer vida = c.GetComponent<VidaPlayer>();
            if (vida)
            {
                attack = true;
                vida.VidaActual -= damage;
            }
        }
    }
}